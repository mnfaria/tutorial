#include "fatorial.h"

int fatorial(int x){
	int fat = 1;
	int i = 1;

	if (x < 0 || x == 0){
		return -1;
    }
    else{
    	for (i = 1; i <= x; i++)
        	fat = fat * i;
    }
    
	return fat;
}